"""A simple desktopography.net wallpaper retriever"""
import importlib.metadata

__version__ = importlib.metadata.version(__package__)
